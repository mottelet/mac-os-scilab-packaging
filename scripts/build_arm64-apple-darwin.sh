#!/usr/bin/env bash
#
# Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
# Copyright (C) Dassault Systemes - 2022 - Clement DAVID
# Copyright (C) 2023-2024 - Stéphane MOTTELET
#
# Builder script for building macOS arm64 version.
#
# NOTE: log all commands to log.txt to avoid hitting Gitlab log limit
# NOTE: nproc is used to limit memory usage

# CI_PROJECT_DIR
# CI_COMMIT_SHA
# CI_COMMIT_SHORT_SHA
# SCI_VERSION_STRING
# SCI_VERSION_TIMESTAMP

LOG_PATH=logs_$CI_COMMIT_SHORT_SHA
[ ! -d "$LOG_PATH" ] && mkdir "$LOG_PATH"

# checkout SDK
MIN_MACOSX_VERSION=11.0
SDK=MacOSX${MIN_MACOSX_VERSION}.sdk
curl -LO https://github.com/phracker/MacOSX-SDKs/releases/download/11.3/${SDK}.tar.xz
tar -xvzf ${SDK}.tar.xz > ${LOG_PATH}/log_prereq_${CI_COMMIT_SHORT_SHA}.txt
export SDKROOT=$(pwd)/${SDK}

# checkout pre-requirements
echo -e "\e[0Ksection_start:$(date +%s):prerequirements\r\e[0KGetting prerequirements"
curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements/prerequirements-scilab-branch-${BRANCH}-macosx.tar.xz
tar -xvzf prerequirements-scilab-branch-${BRANCH}-macosx.tar.xz -C scilab > ${LOG_PATH}/log_prereq_${CI_COMMIT_SHORT_SHA}.txt

# checkout pre-requirements: JavaFX
echo -e "\e[0Ksection_start:$(date +%s):prerequirements\r\e[0KGetting JavaFX"
curl -LO https://download2.gluonhq.com/openjfx/17.0.8/openjfx-17.0.8_osx-aarch64_bin-sdk.zip
unzip -o openjfx-17.0.8_osx-aarch64_bin-sdk.zip
cp javafx-sdk-17.0.8/lib/*.dylib scilab/lib/thirdparty
cp javafx-sdk-17.0.8/lib/javafx.base.jar scilab/thirdparty
cp javafx-sdk-17.0.8/lib/javafx.base.jar scilab/thirdparty
cp javafx-sdk-17.0.8/lib/javafx.swing.jar scilab/thirdparty
cp javafx-sdk-17.0.8/lib/javafx.graphics.jar scilab/thirdparty

echo -e "\e[0Ksection_end:$(date +%s):prerequirements\r\e[0K"

# patch version numbers
sed -i \
 -e "s/SCI_VERSION_STRING .*/SCI_VERSION_STRING \"${SCI_VERSION_STRING}\"/" \
 -e "s/SCI_VERSION_WIDE_STRING .*/SCI_VERSION_WIDE_STRING L\"${SCI_VERSION_STRING}\"/" \
 -e "s/SCI_VERSION_REVISION .*/SCI_VERSION_REVISION \"${CI_COMMIT_SHA}\"/" \
 -e "s/SCI_VERSION_TIMESTAMP .*/SCI_VERSION_TIMESTAMP ${SCI_VERSION_TIMESTAMP}/" \
 scilab/modules/core/includes/version.h.in

# macOS Application bundle folder
APP=$(pwd)/${SCI_VERSION_STRING}.app
DESTDIR=${APP}/Contents
RESOURCES=${APP}/Contents/Resources

# configure (with reconfigure for up to date info)
echo -e "\e[0Ksection_start:$(date +%s):configure[collapsed=true]\r\e[0KConfigure"
cd scilab ||exit 1
aclocal  >  ../${LOG_PATH}/log_autoreconf_${CI_COMMIT_SHORT_SHA}.txt
autoconf >> ../${LOG_PATH}/log_autoreconf_${CI_COMMIT_SHORT_SHA}.txt
automake >> ../${LOG_PATH}/log_autoreconf_${CI_COMMIT_SHORT_SHA}.txt

# set path to prerequisites
export PREREQ_BIN=${PREREQ_PREFIX}/bin
export PREREQ_LIB=${PREREQ_PREFIX}/lib
export PREREQ_INCLUDE=${PREREQ_PREFIX}/include
export PATH=${PREREQ_BIN}:${PATH}
export OCAMLLIB=${PREREQ_LIB}/ocaml
# export CXXFLAGS="-I$PREREQ_INCLUDE $CXXFLAGS"
# export LDFLAGS="-L$PREREQ_LIB -Wl,-rpath,$PREREQ_LIB $LDFLAGS"

./configure --prefix='' \
--without-tk \
--with-eigen-include=$CONDA_PREFIX/include/eigen3 \
--with-matio-include=$CONDA_PREFIX/include \
--with-matio-library=$CONDA_PREFIX/lib \
--with-min_macosx_version=${MIN_MACOSX_VERSION} \
|tee -a ../${LOG_PATH}/log_configure_${CI_COMMIT_SHORT_SHA}.txt
echo -e "\e[0Ksection_end:$(date +%s):configure\r\e[0K"

# make
echo -e "\e[0Ksection_start:$(date +%s):make\r\e[0KMake"
make -j$(sysctl -n hw.physicalcpu) all >> ../${LOG_PATH}/log_build_${CI_COMMIT_SHORT_SHA}.txt 2>&1 ||(tail -n 100 ../${LOG_PATH}/log_build_${CI_COMMIT_SHORT_SHA}.txt; exit 1)
make doc > ../${LOG_PATH}/log_doc_${CI_COMMIT_SHORT_SHA}.txt 2>&1 || (tail -n 100 ../$LOG_PATH/log_doc_${CI_COMMIT_SHORT_SHA}.txt; exit 1) || true
make doc-inline &>"../${LOG_PATH}/build_help_${CI_COMMIT_SHORT_SHA}.log" ||(tail --lines=100 "../$LOG_PATH/build_help_${CI_COMMIT_SHORT_SHA}.log"; exit 1)
echo -e "\e[0Ksection_end:$(date +%s):make\r\e[0K"

# install to ${DESTDIR}
echo -e "\e[0Ksection_start:$(date +%s):install\r\e[0KInstall"
make install DESTDIR="${DESTDIR}" >> ../${LOG_PATH}/log_install_${CI_COMMIT_SHORT_SHA}.txt 2>&1 ||(tail -n 100 ../$LOG_PATH/log_install_${CI_COMMIT_SHORT_SHA}.txt; exit 1)
echo -e "\e[0Ksection_end:$(date +%s):install\r\e[0K"

# install thirdparty headers
cp -a ${CONDA_PREFIX}/include/libxml2 ${DESTDIR}/include
cp -a ${CONDA_PREFIX}/include/eigen3/Eigen ${DESTDIR}/include
mkdir -p ${DESTDIR}/include/nlohmann
cp ${CONDA_PREFIX}/include/nlohmann/json.hpp ${DESTDIR}/include/nlohmann
cp ${CONDA_PREFIX}/include/pcre.h ${DESTDIR}/include

echo -e "\e[0Ksection_start:$(date +%s):patch[collapsed=true]\r\e[0KPatch binary"
# copy extra files
mkdir -p ${RESOURCES}
cp -a ACKNOWLEDGEMENTS "${RESOURCES}/"
cp -a CHANGES.md "${RESOURCES}/"
cp -a COPYING "${RESOURCES}/"
cp -a README.md "${RESOURCES}/"

# Update the classpath
sed -i "s#$(pwd)#\$SCILAB/../../#g" "/${DESTDIR}/share/scilab/etc/classpath.xml"

# Create the application bundle (includes, among other tasks, copy of the conda-forge libraries 
# in lib/thirdparty folder of the bundle), copy of the jre...
cd "${CI_PROJECT_DIR}" ||exit
${TOOLS}/create_scilab_app scilab ${APP}

# copy of the jre
curl -L https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17.0.8.1%2B1/OpenJDK17U-jre_aarch64_mac_hotspot_17.0.8.1_1.tar.gz > jre.tgz
mkdir -p ${APP}/Contents/thirdparty/jre
tar --strip-components=3 -C $APP/Contents/thirdparty/jre -xvzf jre.tgz jdk-17.0.8.1+1-jre/Contents/Home/

echo -e "\e[0Ksection_end:$(date +%s):patch\r\e[0K"

# package as macOS application bundle in a compressed dmg image
echo -e "\e[0Ksection_start:$(date +%s):package\r\e[0KPackage"

# make the dmg image, sign and notarize (to skip signing set SIGN=no)
${TOOLS}/codesign_scilab_app ${APP}
echo -e "\e[0Ksection_end:$(date +%s):package\r\e[0K"

# error if artifact does not exist
if [ "$SIGN" = "yes" ]; then
    du -h "${SCI_VERSION_STRING}-${ARCH}.dmg"
else
    du -h "${SCI_VERSION_STRING}-unsigned-${ARCH}.dmg"
fi
