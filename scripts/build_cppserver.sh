#!/usr/bin/env bash

export SDKROOT=/Users/mottelet/BUILD/scilab/MacOSX11.0.sdk

ARCH=$(uname -m)

SCI_VERSION_STRING=2025.0.0
INSTALLUSRDIR=$(pwd)/../$ARCH/
DOWNLOADDIR=$(pwd)/downloads
BUILDDIR=$(pwd)/builds
LOGDIR=$(pwd)/$SCI_VERSION_STRING

mkdir -p "$INSTALLUSRDIR"
mkdir -p "$INSTALLUSRDIR/lib"
mkdir -p "$INSTALLUSRDIR/bin"
mkdir -p "$INSTALLUSRDIR/include"
mkdir -p "$DOWNLOADDIR"
mkdir -p "$BUILDDIR"
mkdir -p "$LOGDIR"

# CppServer and its deps
CPPSERVER_VERSION=1.0.4.1
ASIO_VERSION=1.29.0
CPPCOMMON_VERSION=1.0.4.0
CPPSERVER_FMT_VERSION=10.2.1
CPPSERVER_CMAKE_VERSION=1.0.0.0

cd $DOWNLOADDIR

    # CppServer and its deps
    [ ! -f cppserver-$CPPSERVER_VERSION.zip ] && curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements-sources/cppserver-$CPPSERVER_VERSION.zip
    [ ! -f asio-$ASIO_VERSION.zip ] && curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements-sources/asio-$ASIO_VERSION.zip
    [ ! -f cppcommon-$CPPCOMMON_VERSION.zip ] && curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements-sources/cppcommon-$CPPCOMMON_VERSION.zip
    [ ! -f cppserverfmt-$CPPSERVER_FMT_VERSION.zip ] && curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements-sources/cppserverfmt-$CPPSERVER_FMT_VERSION.zip
    [ ! -f cppservercmake-$CPPSERVER_FMT_VERSION.zip ] && curl -LO https://oos.eu-west-2.outscale.com/scilab-releases-dev/prerequirements-sources/cppservercmake-$CPPSERVER_CMAKE_VERSION.zip

cd "$BUILDDIR" || exit 1

INSTALL_DIR=$BUILDDIR/cppserver-$CPPSERVER_VERSION/install_dir

rm -rf cppserver
unzip -q "$DOWNLOADDIR/cppserver-$CPPSERVER_VERSION.zip"
mv CppServer-$CPPSERVER_VERSION cppserver
cd cppserver || exit 1
# add cmake files
unzip -q "$DOWNLOADDIR/cppservercmake-$CPPSERVER_CMAKE_VERSION.zip"
mv CppCMakeScripts-$CPPSERVER_CMAKE_VERSION cmake

# add cppserver needed modules
cd modules || exit 1
unzip -q "$DOWNLOADDIR/asio-$ASIO_VERSION.zip"
mv asio-asio-"$(echo $ASIO_VERSION | tr "." "-")" asio
unzip -q "$DOWNLOADDIR/cppcommon-$CPPCOMMON_VERSION.zip"
mv CppCommon-$CPPCOMMON_VERSION CppCommon

curl -L -o  CppCommon/source/string/encoding.cpp https://raw.githubusercontent.com/debugmenot/CppCommon/refs/heads/patch-1/source/string/encoding.cpp

cd CppCommon/modules || exit 1
unzip -q "$DOWNLOADDIR/cppserverfmt-$CPPSERVER_FMT_VERSION.zip"
mv fmt-$CPPSERVER_FMT_VERSION fmt
cd ../../../

# link against our openssl
# export OPENSSL_ROOT_DIR="$INSTALLUSRDIR"

ln -s "$BUILDDIR/cppserver/cmake" modules/CppCommon/cmake

# update cmake files to include only needed sources
sed -i '/Catch2/s/^/#/' modules/CMakeLists.txt
sed -i '/CppBenchmark/s/^/#/' modules/CMakeLists.txt
sed -i '/cpp-optparse/s/^/#/' modules/CMakeLists.txt
sed -i '/Catch2/s/^/#/' modules/CppCommon/modules/CMakeLists.txt
sed -i '/CppBenchmark/s/^/#/' modules/CppCommon/modules/CMakeLists.txt
sed -i '/vld/s/^/#/' modules/CppCommon/modules/CMakeLists.txt

# generate makefile
mkdir -p build
cd build || exit 1
# DCPPSERVER_MODULE: remove execution of benchmarks and tests from build
cmake .. -DCMAKE_INSTALL_PREFIX="$INSTALL_DIR" \
    -DBUILD_SHARED_LIBS=ON \
    -DCPPSERVER_MODULE=true \
    -G "Unix Makefiles"
# build cppserver
cmake --build . --parallel --config Release

# copy libs
cd ../
cp -- $(find . -name *.dylib) "$INSTALLUSRDIR/lib/"

# copy includes
rm -rf "$INSTALLUSRDIR/include/cppserver/"
mkdir -p "$INSTALLUSRDIR/include/cppserver/cppserver"
mkdir -p "$INSTALLUSRDIR/include/cppserver/cppcommon"
mkdir -p "$INSTALLUSRDIR/include/cppserver/asio"
mkdir -p "$INSTALLUSRDIR/include/cppserver/fmt"

cp -r include/server "$INSTALLUSRDIR/include/cppserver/cppserver"

rm modules/asio/asio/include/Makefile.am
cp -r modules/asio/asio/include/* "$INSTALLUSRDIR/include/cppserver/asio/"

cp -r modules/CppCommon/include/* "$INSTALLUSRDIR/include/cppserver/cppcommon/"
cp -r modules/CppCommon/modules/fmt/include/* "$INSTALLUSRDIR/include/cppserver/fmt/"

# fix rpaths in dylibs
dylibs=`find $INSTALLUSRDIR/lib -name '*.dylib' -type f`
for adylib in $dylibs; do
    rpaths=`otool -l $adylib | awk '2 == gsub(/^ +path | \(offset [0-9]+\)$/, "")'`
    for arpath in $rpaths; do
        if [ "$arpath" != "$CONDA_PREFIX/lib" ]; then
            install_name_tool -delete_rpath "$arpath" "$adylib"
        fi
    done
    install_name_tool -add_rpath @loader_path "$adylib"
done

