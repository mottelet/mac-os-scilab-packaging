#!/usr/bin/env bash
#
# Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
# Copyright (C) 2023-2024 - Stéphane MOTTELET

# Builder script for building Scilab.

export TAG=minor
export VER=2025.1.0
git clone --depth 1 --branch ${TAG} https://gitlab.com/scilab/scilab.git scilab
cd scilab

export ARCH=$(uname -m)
export SCI_VERSION_STRING=scilab-${VER}
export CI_PROJECT_DIR=$(pwd)
export CI_COMMIT_SHORT_SHA=$(git rev-parse --short HEAD)
export SCI_VERSION_TIMESTAMP=$(git show --no-patch --format=%ct HEAD)
export BRANCH=minor
export SIGN=no

# install packaging tools
git clone --depth 1 --branch ${TAG} https://gitlab.com/mottelet/mac-os-scilab-packaging
export TOOLS=$(pwd)/mac-os-scilab-packaging
export PREREQ_PREFIX=${TOOLS}/${ARCH}

# install miniconda
export CONDA_VER=py311_24.7.1-0
rm -rf miniconda

# download installer
curl -LO https://repo.anaconda.com/miniconda/Miniconda3-${CONDA_VER}-MacOSX-${ARCH}.sh

# install and activate base environment
sh ./Miniconda3-${CONDA_VER}-MacOSX-${ARCH}.sh -b -p ./miniconda
./miniconda/bin/conda init bash
source ~/.bash_profile

# create and activate a fresh conda scilab build environment
ENV=scilab_build_${TAG}_${ARCH}
conda deactivate
conda remove -y --name ${ENV} --all || true
conda create --name ${ENV} --file ${TOOLS}/environment_${ARCH}.txt
conda activate ${ENV}

source ${TOOLS}/scripts/build_${ARCH}-apple-darwin.sh

