/* runtime/caml/version.h.  Generated from version.h.in by configure.  */
/**************************************************************************/
/*                                                                        */
/*                                 OCaml                                  */
/*                                                                        */
/*             Sebastien Hinderer, projet Cambium, INRIA Paris            */
/*                                                                        */
/*   Copyright 2021 Institut National de Recherche en Informatique et     */
/*     en Automatique.                                                    */
/*                                                                        */
/*   All rights reserved.  This file is distributed under the terms of    */
/*   the GNU Lesser General Public License version 2.1, with the          */
/*   special exception on linking described in the file LICENSE.          */
/*                                                                        */
/**************************************************************************/

/* Macros defining the current version of OCaml */

#define OCAML_VERSION_MAJOR 4
#define OCAML_VERSION_MINOR 14
#define OCAML_VERSION_PATCHLEVEL 2
#define OCAML_VERSION_ADDITIONAL "dev0-2022-12-19"
#define OCAML_VERSION_EXTRA "dev0-2022-12-19"
#define OCAML_VERSION 41402
#define OCAML_VERSION_STRING "4.14.2+dev0-2022-12-19"
